﻿using ProjectStructure.Common.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.Common.DTOs.Task
{
    public class TaskUpdateDTO
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public TaskState State { get; set; }
        public DateTime? FinishedAt { get; set; }
        [Required]
        public int ProjectId { get; set; }
        [Required]
        public int PerformerId { get; set; }
    }
}
