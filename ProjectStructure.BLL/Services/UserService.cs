﻿using AutoMapper;
using ProjectStructure.BLL.Contracts;
using ProjectStructure.Common.DTOs.User;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class UserService : BaseService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IMapper mapper, IUserRepository userRepository) : base(mapper)
        {
            _userRepository = userRepository;
        }

        public async Task<List<UserDTO>> GetAll()
        {
            var users = await _userRepository.GetAll();
            return _mapper.Map<List<UserDTO>>(users);
        }

        public async Task<UserDTO> GetUserById(int id)
        {
            var user = await _userRepository.GetById(id);
            if (user == null)
            {
                throw new Exception("The user is not found");
            }

            return _mapper.Map<UserDTO>(user);
        }

        public async Task<UserDTO> CreateUser(UserCreateDTO userCreateDTO)
        {
            var userEntity = _mapper.Map<User>(userCreateDTO);
            //userEntity.Id = (_userRepository.GetAll().Max(u => u.Id)) + 1;
            userEntity.RegisteredAt = DateTime.Now;
            await _userRepository.Add(userEntity);

            return _mapper.Map<UserDTO>(userEntity);
        }

        public async System.Threading.Tasks.Task UpdateUser(UserUpdateDTO userUpdateDTO)
        {
            var entity = await GetUserById(userUpdateDTO.Id);
            var updatedEntity = _mapper.Map<User>(userUpdateDTO);
            updatedEntity.RegisteredAt = entity.RegisteredAt;

            await _userRepository.Update(updatedEntity);
        }

        public async System.Threading.Tasks.Task DeleteUser(int id)
        {
            await GetUserById(id);
            await _userRepository.Delete(id);
        }
    }
}
