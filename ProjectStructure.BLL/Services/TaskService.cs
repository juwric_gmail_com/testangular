﻿using AutoMapper;
using ProjectStructure.BLL.Contracts;
using ProjectStructure.Common.DTOs.Task;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class TaskService : BaseService
    {
        private readonly ITaskRepository _taskRepository;

        public TaskService(IMapper mapper, ITaskRepository taskRepository) : base(mapper)
        {
            _taskRepository = taskRepository;
        }

        public async Task<List<TaskDTO>> GetAll()
        {
            var tasks = await _taskRepository.GetAll();
            return _mapper.Map<List<TaskDTO>>(tasks);
        }

        public async Task<TaskDTO> GetTaskById(int id)
        {
            var task = await _taskRepository.GetById(id);
            if (task == null)
            {
                throw new Exception("The task is not found");
            }

            return _mapper.Map<TaskDTO>(task);
        }

        public async Task<TaskDTO> CreateTask(TaskCreateDTO taskCreateDTO)
        {
            var taskEntity = _mapper.Map<ProjectStructure.DAL.Models.Task>(taskCreateDTO);
            //taskEntity.Id = (_taskRepository.GetAll().Max(u => u.Id)) + 1;
            taskEntity.CreatedAt = DateTime.Now;
            await _taskRepository.Add(taskEntity);

            return _mapper.Map<TaskDTO>(taskEntity);
        }

        public async System.Threading.Tasks.Task UpdateTask(TaskUpdateDTO taskUpdateDTO)
        {
            var entity = await GetTaskById(taskUpdateDTO.Id);
            var updatedEntity = _mapper.Map<ProjectStructure.DAL.Models.Task>(taskUpdateDTO);
            updatedEntity.CreatedAt = entity.CreatedAt;

            await _taskRepository.Update(updatedEntity);
        }

        public async System.Threading.Tasks.Task DeleteTask(int id)
        {
            await GetTaskById(id);
            await _taskRepository.Delete(id);
        }
    }
}
