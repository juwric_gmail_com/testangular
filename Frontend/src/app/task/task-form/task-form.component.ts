import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IProject } from 'src/app/core/models/IProject';
import { ITask } from 'src/app/core/models/ITask';
import { IUser } from 'src/app/core/models/IUser';
import { PerformerService } from 'src/app/core/services/performer.service';
import { ProjectService } from 'src/app/core/services/project.service';
import { TaskService } from 'src/app/core/services/task.service';
import { TeamService } from 'src/app/core/services/team.service';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss'],
})
export class TaskFormComponent implements OnInit {
  taskForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
  });

  errors: any = null;
  task!: ITask;
  state!: number;

  projects!: IProject[];
  performers!: IUser[];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private taskService: TaskService,
    private projectService: ProjectService,
    private performerService: PerformerService
  ) {}

  ngOnInit(): void {
    this.projectService.findAll().subscribe((data) => {
      this.projects = data;
    });
    this.performerService.findAll().subscribe((data) => {
      this.performers = data;
    });

    this.setTaskToForm();
  }

  setTaskToForm(): void {
    this.route.params.subscribe((params) => {
      const id = params['id'];
      if (!id) {
        return;
      }

      this.taskService.findOne(+id).subscribe((data) => {
        this.task = data;
        this.taskForm.patchValue({
          name: data.name,
          description: data.description,
        });
      });
    });
  }

  submitForm() {
    if (this.task) {
      this.taskService
        .update(this.task.id, {
          ...this.taskForm.value,
          projectId: this.task.projectId,
          performerId: this.task.performerId,
        })
        .subscribe(() => {
          this.router.navigateByUrl('/tasks');
        });
      return;
    }
  }

  onChangeTask(formValue: any) {
    this.task.projectId = formValue.value;
  }

  onChangeUser(formValue: any) {
    this.task.performerId = formValue.value;
  }
}
