import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanDeactivate,
} from '@angular/router';
import { Observable } from 'rxjs';
import { TeamFormComponent } from '../team/team-form/team-form.component';

export class SaveGuard implements CanDeactivate<TeamFormComponent> {
  canDeactivate(
    component: TeamFormComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState: RouterStateSnapshot
  ): Observable<boolean> | boolean {
    if (component.teamForm.value.name != component.team.name) {
      return confirm('Are you sure you want to switch?');
    } else {
      return true;
    }
  }
}
