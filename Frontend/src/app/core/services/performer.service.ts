import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ITask } from '../models/ITask';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class PerformerService {
  constructor(private apiService: ApiService) {}

  findAll(): Observable<any> {
    return this.apiService.get('/users');
  }
}
