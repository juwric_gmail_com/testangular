import { IBase } from './IBase';
import { IProject } from './IProject';
import { IUser } from './IUser';

export interface ITask extends IBase {
  name: string;
  description: string;
  state: TaskState;
  createdAt: string;
  finishedAt: string | null;
  projectId: number;
  project: IProject;
  performerId: number | null;
  performer: IUser;
}

export enum TaskState {
  Finished,
  UnFinished,
  Canceled,
  InProcess,
}
