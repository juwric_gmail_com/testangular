import { IBase } from './IBase';
import { ITeam } from './ITeam';

export interface IUser extends IBase {
  firstName: string;
  lastName: string;
  email: string;
  registeredAt: string;
  birthDay: string;
  website: string;
  company: string;
  teamId: number | null;
  team: ITeam;
}
