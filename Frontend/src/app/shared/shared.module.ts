import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { FormatPipe } from './format.pipe';
import { BoldDirective } from './bold.directive';

@NgModule({
  declarations: [HeaderComponent, FormatPipe, FormatPipe, BoldDirective],
  imports: [CommonModule, RouterModule],
  exports: [HeaderComponent, FormatPipe, BoldDirective],
})
export class SharedModule {}
