import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'format',
})
export class FormatPipe implements PipeTransform {
  transform(value: string, args?: any): any {
    var now = new Date(value);
    return (
      now.getDay() +
      ' ' +
      now.toLocaleString('ukr', {
        month: 'long',
      }) +
      ' ' +
      now.getFullYear()
    );
  }
}
