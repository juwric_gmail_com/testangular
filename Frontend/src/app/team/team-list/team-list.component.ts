import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ITeam } from 'src/app/core/models/ITeam';
import { TeamService } from 'src/app/core/services/team.service';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss'],
})
export class TeamListComponent {
  teams: ITeam[] = [];
  constructor(private teamService: TeamService, private router: Router) {}

  ngOnInit(): void {
    this.teamService.findAll().subscribe((res) => {
      this.teams = res;
    });
  }

  editTeam(teamId: number): void {
    this.router.navigate(['teams', 'edit', teamId]);
  }

  deleteTeam(teamId: number): void {
    this.teams = this.teams.filter((t) => t.id != teamId);
    this.teamService.delete(teamId).subscribe((res) => {
      //this.teams = this.teams.filter((t) => t.id != teamId);
    });
  }
}
