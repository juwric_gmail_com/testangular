import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ITeam } from 'src/app/core/models/ITeam';
import { TeamService } from 'src/app/core/services/team.service';

@Component({
  selector: 'app-team-form',
  templateUrl: './team-form.component.html',
  styleUrls: ['./team-form.component.scss'],
})
export class TeamFormComponent implements OnInit {
  teamForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
  });

  errors: any = null;
  team!: ITeam;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private teamService: TeamService
  ) {}

  ngOnInit(): void {
    this.setTeamToForm();
  }

  setTeamToForm(): void {
    this.route.params.subscribe((params) => {
      const id = params['id'];
      if (!id) {
        return;
      }

      this.teamService.findOne(+id).subscribe((data) => {
        this.team = data;
        this.teamForm.patchValue({
          name: data.name,
        });
      });
    });
  }

  submitForm() {
    if (this.team) {
      this.teamService
        .update(this.team.id, this.teamForm.value)
        .subscribe(() => {
          this.router.navigateByUrl('/teams');
        });
      return;
    }
  }
}
