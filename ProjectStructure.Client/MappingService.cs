﻿using CollectionsLINQ.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace CollectionsLINQ
{
    public class MappingService
    {
        private HttpClient _client;
        public static List<Project> projects = new List<Project>();
        public static List<Models.Task> tasks = new List<Models.Task>();
        public static List<Team> teams = new List<Team>();
        public static List<User> users = new List<User>();
        private readonly string _host;

        public MappingService(string host)
        {
            _client = new HttpClient();
            _host = host;
        }

        public async System.Threading.Tasks.Task Init()
        {
            System.Threading.Tasks.Task[] taskArray = { GetUsers(), GetTeams(), GetTasks(), GetProjects() };
            await System.Threading.Tasks.Task.WhenAll(taskArray);
        }

        public IEnumerable<Project> GetProjectList()
        {
            var projectsMapping = from project in projects
                                  join user in users on project.AuthorId equals user.Id
                                  join team in teams on project.TeamId equals team.Id
                                  select new Project
                                  {
                                      Name = project.Name,
                                      Description = project.Description,
                                      Deadline = project.Deadline,
                                      CreatedAt = project.CreatedAt,
                                      AuthorId = project.AuthorId,
                                      Author = user,
                                      TeamId = project.TeamId,
                                      Team = team
                                  };

            var tasksMapping =
               from task in tasks
               join user in users on task.PerformerId equals user.Id
               select new Models.Task
               {
                   Id = task.Id,
                   ProjectId = task.ProjectId,
                   PerformerId = task.PerformerId,
                   Performer = user,
                   Name = task.Name,
                   Description = task.Description,
                   State = task.State,
                   CreatedAt = task.CreatedAt,
                   FinishedAt = task.FinishedAt
               };

            var result = projectsMapping.GroupJoin(tasksMapping,
                project => project.Id,
                task => task.ProjectId,
                (project, task) => new Project
                {
                    Name = project.Name,
                    Description = project.Description,
                    Deadline = project.Deadline,
                    CreatedAt = project.CreatedAt,
                    AuthorId = project.AuthorId,
                    Author = project.Author,
                    TeamId = project.TeamId,
                    Team = project.Team,
                    Tasks = task.Select(t => t)
                });

            return result;
        }


        public async System.Threading.Tasks.Task GetUsers()
        {
            var usersStr = await _client.GetStringAsync(_host + "/api/Users");
            var usersObj = JsonConvert.DeserializeObject<List<User>>(usersStr);
            users = usersObj;
        }

        public async System.Threading.Tasks.Task GetTeams()
        {
            var teamsStr = await _client.GetStringAsync(_host + "/api/Teams");
            var teamsObj = JsonConvert.DeserializeObject<List<Team>>(teamsStr);
            teams = teamsObj;
        }

        public async System.Threading.Tasks.Task<List<Task>> GetTasks()
        {
            var tasksStr = await _client.GetStringAsync(_host + "/api/Tasks");
            var tasksObj = JsonConvert.DeserializeObject<List<Models.Task>>(tasksStr);
            tasks = tasksObj;
            return tasks;
        }

        public async System.Threading.Tasks.Task GetProjects()
        {
            var projectsStr = await _client.GetStringAsync(_host + "/api/Projects");
            var projectsObj = JsonConvert.DeserializeObject<List<Project>>(projectsStr);
            projects = projectsObj;
        }

        public async System.Threading.Tasks.Task UpdateTask(Task task)
        {
            var jsonTask = JsonConvert.SerializeObject(task);
            var requestContent = new StringContent(jsonTask, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync(_host + "/api/Tasks", requestContent);
            response.EnsureSuccessStatusCode();
        }
    }
}
