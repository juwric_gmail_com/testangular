﻿using CollectionsLINQ;
using CollectionsLINQ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Client
{
    public class Queries
    {
        private readonly IEnumerable<Project> _list;
        private readonly MappingService _mappingService;
        private readonly TaskCompletionSource<int> _tcRandomTask = new TaskCompletionSource<int>();
        private readonly Timer _timer = null;

        public Queries(IEnumerable<Project> list, MappingService service)
        {
            _list = list;
            _mappingService = service;
        }

        public Dictionary<Project, int> GetTasksSpecificUser(int userId = 29)
        {
            return _list.Where(q => q.AuthorId == userId)
                .ToDictionary(p => p, t => t.Tasks.Count());
        }

        public List<CollectionsLINQ.Models.Task> GetTasksAssigned(int userId = 34)
        {
            var result = _list.SelectMany(p => p.Tasks)
                .Where(t => t.PerformerId == userId && t.Name.Length < 45);

            return result.ToList();
        }

        public List<dynamic> GetListTasksCompleted(int userId = 34)
        {
            var result = _list.SelectMany(p => p.Tasks)
                .Where(t => t.PerformerId == userId && t.State == TaskState.Finished && t.FinishedAt?.Year == 2021)
                .Select(t => new { id = t.Id, name = t.Name });

            return result.ToList<dynamic>();
        }

        public List<dynamic> GetListWihtIdTeamName()
        {
            int year = DateTime.Now.Year;
            var result = MappingService.teams.GroupJoin(
               MappingService.users,
                team => team.Id,
                user => user.TeamId,
                (team, user) => new
                {
                    id = team.Id,
                    name = team.Name,
                    users = user
                    .OrderByDescending(u => u.RegisteredAt)
                    .Where(u => (year - u.BirthDay.Year) > 10)
                });

            return result.ToList<dynamic>();
        }

        public List<dynamic> GetListOfUsersAlphabetically()
        {
            var result = MappingService.users.OrderBy(u => u.FirstName).GroupJoin(
                MappingService.tasks,
                user => user.Id,
                task => task.PerformerId,
                (user, task) => new
                {
                    first_name = user.FirstName,
                    tasks = task.OrderByDescending(t => t.Name.Length)
                });

            return result.ToList<dynamic>();
        }

        public List<dynamic> GetStructure1(int userId = 34)
        {
            var result = MappingService.users.Where(u => u.Id == userId).GroupJoin(MappingService.projects,
                u => u.Id,
                p => p.AuthorId,
                (u, p) => new
                {
                    user = u,
                    lastProject = p.OrderBy(lp => lp.CreatedAt).First(),
                }).GroupJoin(MappingService.tasks,
                    prev => prev.user.Id,
                    t => t.PerformerId,
                    (prev, t) => new
                    {
                        user = prev.user,
                        lastProject = prev.lastProject,
                        totalUnFinishedOrCanceled = t.Where(t =>
                            t.State == TaskState.UnFinished || t.State == TaskState.Canceled),
                        longestTask = t.OrderByDescending(t => t.FinishedAt - t.CreatedAt).First()
                    });

            return result.ToList<dynamic>();
        }

        public List<dynamic> GetStructure2()
        {
            var result = MappingService.projects.Select(p => new
            {
                project = p,
                longTask = p.Tasks.OrderByDescending(t => t.Description.Length).First(),
                shortTask = p.Tasks.OrderBy(t => t.Name.Length).First(),
            });

            return null;
        }

        public System.Threading.Tasks.Task<int> MarkRandomTaskWithDelay(int delay)
        {
            TimerCallback tm = new TimerCallback(MarkRandomTask);
            Timer timer = new Timer(tm, null, 0, delay);
            return _tcRandomTask.Task;
        }

        private void MarkRandomTask(object obj)
        {
            System.Threading.Tasks.Task.Run(async () =>
            {
                try
                {
                    var tasks = await _mappingService.GetTasks();
                    int index = new Random().Next(tasks.Count);
                    var randomTask = tasks[index];

                    randomTask.State = TaskState.Finished;
                    await _mappingService.UpdateTask(randomTask);
                    _tcRandomTask.SetResult(randomTask.Id);
                }
                catch (Exception ex)
                {
                    _tcRandomTask.SetException(ex);
                }
                finally
                {
                    if (_timer != null)
                    {
                        _timer.Dispose();
                    }
                }
            });
        }
    }
}
