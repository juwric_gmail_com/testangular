﻿using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.Contracts
{
    public interface ITeamRepository : IBaseRepository<Team>
    {
    }
}
