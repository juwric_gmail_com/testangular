﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.DAL.Models
{
    public class Team : BaseEntity
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
