﻿using Microsoft.AspNetCore.Mvc;

namespace ProjectStructure.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : Controller
    {

    }
}
