﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common.DTOs.Task;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.Presentation.Controllers
{
    public class TasksController : BaseController
    {
        private readonly TaskService _taskService;

        public TasksController(TaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public async Task<ActionResult<List<TaskDTO>>> Get()
        {
            return Ok(await _taskService.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> GetById(int id)
        {
            return Ok(await _taskService.GetTaskById(id));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateTask([FromBody] TaskUpdateDTO task)
        {
            await _taskService.UpdateTask(task);
            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> CreateTask([FromBody] TaskCreateDTO task)
        {
            await _taskService.CreateTask(task);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _taskService.DeleteTask(id);
            return NoContent();
        }
    }
}
