﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common.DTOs.Team;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.Presentation.Controllers
{
    public class TeamsController : BaseController
    {
        private readonly TeamService _teamService;

        public TeamsController(TeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<List<TeamDTO>>> Get()
        {
            return Ok(await _teamService.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> GetById(int id)
        {
            return Ok(await _teamService.GetUserById(id));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateTeam([FromBody] TeamUpdateDTO user)
        {
            await _teamService.UpdateUser(user);
            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> CreateTeam([FromBody] TeamCreateDTO user)
        {
            await _teamService.CreateUser(user);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _teamService.DeleteUser(id);
            return NoContent();
        }
    }
}
